﻿using Enumerations;
using IniParser;
using IniParser.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agents
{
    class Program
    {
        static string BOARD_FILENAME = "board.txt";
        static string INI_FILENAME = "conf.ini";
        static int DEFAULT_N_PLY = 5;
        static Heuristic DEFAULT_HEURISTIC = Heuristic.COMPLETE;

        static int Main(string[] args)
        {
            Board.SetupPositionValues();

            int nPly;
            Heuristic h;
            var parser = new FileIniDataParser();
            IniData ini;
            if (File.Exists(INI_FILENAME))
            {
                ini = parser.ReadFile(INI_FILENAME);
                nPly = int.Parse(ini["Configurations"]["N_PLY"]);
                h = (Heuristic)Enum.Parse(typeof(Heuristic), ini["Configurations"]["HEURISTIC"]);
            }
            else
            {
                ini = new IniData();
                nPly = DEFAULT_N_PLY;
                h = DEFAULT_HEURISTIC;
                ini.Sections.AddSection("Configurations");
                ini["Configurations"].AddKey("N_PLY", DEFAULT_N_PLY.ToString());
                ini["Configurations"].AddKey("HEURISTIC", DEFAULT_HEURISTIC.ToString());
                parser.WriteFile(INI_FILENAME, ini);
            }

            Board.HEURISTIC = h;

            Board b = GetBoardFromFile();
            Minimax mm;
            if (args[0] == "B")
            {
                mm = new Minimax(b, Enumerations.Color.BLACK);
            }
            else
            {
                mm = new Minimax(b, Enumerations.Color.WHITE);
            }
            Board boardAfterMove = mm.FindNextMove(nPly);
            if (boardAfterMove == null)
            {
                return -1;
            }else
            {
                DumpBoard(boardAfterMove);
                return 0;
            }
        }

        static Board GetBoardFromFile()
        {
            var lines = File.ReadAllLines(BOARD_FILENAME);
            Board b = new Board(lines[0]);
            return b;
        }

        static void DumpBoard(Board b)
        {
            using (var sw = new StreamWriter(BOARD_FILENAME, false))
            {
                sw.Write(b.ParameterString);
            }

        }
    }
}
