﻿using Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agents
{
    class Board
    {

        #region Static
        public static Heuristic HEURISTIC;
        private static Dictionary<Position, int> _Base_Position_Values;
        private static void AddSinglePosition(Position p, int val)
        {
            try
            {
                _Base_Position_Values.Add(p, val);
                _Base_Position_Values.Add(new Position(p.X, BOARD_HEIGHT - 1 - p.Y), val);
                _Base_Position_Values.Add(new Position(BOARD_WIDTH - 1 - p.X, p.Y), val);
                _Base_Position_Values.Add(new Position(BOARD_WIDTH - 1 - p.X, BOARD_HEIGHT - 1 - p.Y), val);
                Position inverted = new Position(p.Y, p.X);
                _Base_Position_Values.Add(inverted, val);
                _Base_Position_Values.Add(new Position(inverted.X, BOARD_HEIGHT - 1 - inverted.Y), val);
                _Base_Position_Values.Add(new Position(BOARD_WIDTH - 1 - inverted.X, inverted.Y), val);
                _Base_Position_Values.Add(new Position(BOARD_WIDTH - 1 - inverted.X, BOARD_HEIGHT - 1 - inverted.Y), val);
            }
            catch (Exception ex)
            {

            }
        }
        public static void SetupPositionValues()
        {
            _Base_Position_Values = new Dictionary<Position, int>();
            AddSinglePosition(new Position(0, 0), 99);
            AddSinglePosition(new Position(1, 0), -8);
            AddSinglePosition(new Position(2, 0), 8);
            AddSinglePosition(new Position(3, 0), 6);
            AddSinglePosition(new Position(1, 1), -24);
            AddSinglePosition(new Position(2, 1), -4);
            AddSinglePosition(new Position(3, 1), -3);
            AddSinglePosition(new Position(2, 2), 7);
            AddSinglePosition(new Position(3, 2), 4);
            AddSinglePosition(new Position(3, 3), 0);
        }

        
        #endregion

        #region Private Variables
        private const int BOARD_HEIGHT = 8;
        private const int BOARD_WIDTH = 8;
        private Piece[,] _board;
        private Dictionary<Color, HashSet<Move>> _valid_moves;
        private Dictionary<Color, int> _number_of_pieces;
        private Move _last_applied_move;
        private int _heuristic_value;
        private int _pieces_flipped_from_last_move;
        private int _turns_passed;
        #endregion

        #region Properties

        public string ParameterString
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                for (int y = 0; y < BOARD_HEIGHT; ++y)
                {
                    for (int x = 0; x < BOARD_WIDTH; ++x)
                    {
                        if (_board[x, y] == null)
                        {
                            sb.Append("0");
                        }
                        else
                        {
                            switch (_board[x, y].CurrentColor)
                            {
                                case Color.BLACK:
                                    sb.Append("B");
                                    break;
                                case Color.WHITE:
                                    sb.Append("W");
                                    break;
                            }
                        }
                    }
                }
                return sb.ToString();
            }
        }

        public int HeuristicValue
        {
            get
            {
                return _heuristic_value;
            }
        }

        public Move LastAppliedMove
        {
            get
            {
                return _last_applied_move;
            }
        }
        #endregion

        #region Constructors
        public Board()
        {
            _heuristic_value = int.MaxValue;
            _board = new Piece[8, 8];
            _board[3, 3] = new Piece(Color.WHITE);
            _board[4, 4] = new Piece(Color.WHITE);
            _board[4, 3] = new Piece(Color.BLACK);
            _board[3, 4] = new Piece(Color.BLACK);
            _valid_moves = new Dictionary<Color, HashSet<Move>>();
            _valid_moves.Add(Color.BLACK, new HashSet<Move>());
            _valid_moves.Add(Color.WHITE, new HashSet<Move>());
            UpdateValidMoves();
            _turns_passed = 0;
        }

        public Board(string initialBoard)
        {
            _heuristic_value = int.MaxValue;
            _board = new Piece[BOARD_WIDTH, BOARD_HEIGHT];
            _number_of_pieces = new Dictionary<Color, int>();
            _number_of_pieces.Add(Color.BLACK, 0);
            _number_of_pieces.Add(Color.WHITE, 0);
            _valid_moves = new Dictionary<Color, HashSet<Move>>();
            _valid_moves.Add(Color.BLACK, new HashSet<Move>());
            _valid_moves.Add(Color.WHITE, new HashSet<Move>());
            int nbPieces = 0;
            for (int y = 0; y < BOARD_HEIGHT; ++y)
            {
                for (int x = 0; x < BOARD_WIDTH; ++x)
                {
                    if (initialBoard[y * BOARD_WIDTH + x] != '0')
                    {
                        if (initialBoard[y * BOARD_WIDTH + x] == 'B')
                        {
                            _board[x, y] = new Piece(Color.BLACK);
                            ++_number_of_pieces[Color.BLACK];
                        }
                        else
                        {
                            _board[x, y] = new Piece(Color.WHITE);
                            ++_number_of_pieces[Color.WHITE];
                        }
                        ++nbPieces;
                    }
                }
            }
            UpdateValidMoves();
            _turns_passed = nbPieces - 4;
        }
        #endregion

        #region Private Functions
        private void UpdateValidMoves()
        {
            _valid_moves[Color.BLACK].Clear();
            _valid_moves[Color.WHITE].Clear();
            for (int y = 0; y < BOARD_HEIGHT; ++y)
            {
                for (int x = 0; x < BOARD_WIDTH; ++x)
                {
                    if (_board[x, y] == null)
                    {
                        Position p = new Position(x, y);
                        AddAnyValidMove(Color.BLACK, p, Direction.UP);
                        AddAnyValidMove(Color.BLACK, p, Direction.DOWN);
                        AddAnyValidMove(Color.BLACK, p, Direction.RIGHT);
                        AddAnyValidMove(Color.BLACK, p, Direction.LEFT);
                        AddAnyValidMove(Color.BLACK, p, Direction.DIAG_LU);
                        AddAnyValidMove(Color.BLACK, p, Direction.DIAG_RU);
                        AddAnyValidMove(Color.BLACK, p, Direction.DIAG_LD);
                        AddAnyValidMove(Color.BLACK, p, Direction.DIAG_RD);
                        AddAnyValidMove(Color.WHITE, p, Direction.UP);
                        AddAnyValidMove(Color.WHITE, p, Direction.DOWN);
                        AddAnyValidMove(Color.WHITE, p, Direction.RIGHT);
                        AddAnyValidMove(Color.WHITE, p, Direction.LEFT);
                        AddAnyValidMove(Color.WHITE, p, Direction.DIAG_LU);
                        AddAnyValidMove(Color.WHITE, p, Direction.DIAG_RU);
                        AddAnyValidMove(Color.WHITE, p, Direction.DIAG_LD);
                        AddAnyValidMove(Color.WHITE, p, Direction.DIAG_RD);
                    }

                }
            }
        }

        private Dictionary<Color, int> ComputeFrontier()
        {
            Dictionary<Color, int> res = new Dictionary<Color, int>();
            res.Add(Color.BLACK, 0);
            res.Add(Color.WHITE, 0);
            for (int y = 0; y < BOARD_HEIGHT; ++y)
            {
                for (int x = 0; x < BOARD_WIDTH; ++x)
                {
                    if (_board[x, y] != null)
                    {
                        Position p = new Position(x, y);
                        if (IsFrontier(p, Direction.DIAG_LD) ||
                            IsFrontier(p, Direction.DIAG_RD) ||
                            IsFrontier(p, Direction.DIAG_LU) ||
                            IsFrontier(p, Direction.DIAG_RU) ||
                            IsFrontier(p, Direction.LEFT) ||
                            IsFrontier(p, Direction.RIGHT) ||
                            IsFrontier(p, Direction.DOWN) ||
                            IsFrontier(p, Direction.UP))
                        {
                            res[_board[x, y].CurrentColor] += 2;
                        }
                    }
                }
            }
            return res;
        }

        private Dictionary<Color, int> ComputeStable()
        {
            Dictionary<Color, int> res = new Dictionary<Color, int>();
            res.Add(Color.BLACK, 0);
            res.Add(Color.WHITE, 0);
            for (int y = 0; y < BOARD_HEIGHT; ++y)
            {
                for (int x = 0; x < BOARD_WIDTH; ++x)
                {
                    if (_board[x, y] != null)
                    {
                        Position p = new Position(x, y);
                        if (CantBeFlipped(_board[x, y].CurrentColor, p, Direction.UP, Direction.DOWN) &&
                            CantBeFlipped(_board[x, y].CurrentColor, p, Direction.DIAG_RU, Direction.DIAG_LD) &&
                            CantBeFlipped(_board[x, y].CurrentColor, p, Direction.RIGHT, Direction.LEFT) &&
                            CantBeFlipped(_board[x, y].CurrentColor, p, Direction.DIAG_RD, Direction.DIAG_LU))
                        {
                            res[_board[x, y].CurrentColor] += 10;
                        }
                    }
                }
            }
            return res;
        }

        private Dictionary<IslandSize, int> ComputeIslands()
        {
            Dictionary<IslandSize, int> res = new Dictionary<IslandSize, int>();
            res.Add(IslandSize.EVEN, 0);
            res.Add(IslandSize.ODD, 0);
            HashSet<Position> explored = new HashSet<Position>();

            for (int y = 0; y < BOARD_HEIGHT; ++y)
            {
                for (int x = 0; x < BOARD_WIDTH; ++x)
                {
                    if (_board[x,y] == null)
                    {
                        Position p = new Position(x, y);
                        if (!explored.Contains(p))
                        {
                            int islandSize = GetIslandRecursive(p, explored);
                            if (islandSize % 2 == 0)
                            {
                                res[IslandSize.EVEN] += 1;
                            }
                            else
                            {
                                res[IslandSize.ODD] += 1;
                            }
                        }
                    }
                }
            }

            return res;
        }

        private int GetIslandRecursive(Position p, HashSet<Position> explored)
        {
            int res = 1;
            explored.Add(p);
            Position np = p.Move(Direction.LEFT);
            if(np != null && _board[np.X,np.Y] == null && !explored.Contains(np))
            {
                res += GetIslandRecursive(np, explored);
            }

            np = p.Move(Direction.UP);
            if (np != null && _board[np.X, np.Y] == null && !explored.Contains(np))
            {
                res += GetIslandRecursive(np, explored);
            }

            np = p.Move(Direction.RIGHT);
            if (np != null && _board[np.X, np.Y] == null && !explored.Contains(np))
            {
                res += GetIslandRecursive(np, explored);
            }

            np = p.Move(Direction.DOWN);
            if (np != null && _board[np.X, np.Y] == null && !explored.Contains(np))
            {
                res += GetIslandRecursive(np, explored);
            }

            return res;
        }

        private bool IsFrontier(Position p, Direction d)
        {
            Position np = p.Move(d);
            if (np != null)
            {
                return _board[np.X, np.Y] == null;
            }
            return false;
        }

        private bool CantBeFlipped(Color c, Position p, Direction d1, Direction d2)
        {
            Position np1 = p.Move(d1);
            while (np1 != null && _board[np1.X, np1.Y] != null && _board[np1.X, np1.Y].CurrentColor == c)
            {
                np1 = np1.Move(d1);
            }
            if (np1 == null)
            {
                return true;
            }
            Position np2 = p.Move(d2);
            while (np2 != null && _board[np2.X, np2.Y] != null && _board[np2.X, np2.Y].CurrentColor == c)
            {
                np2 = np2.Move(d2);
            }
            if (np2 == null)
            {
                return true;
            }
            else
            {
                while (np1 != null && _board[np1.X, np1.Y] != null)
                {
                    np1 = np1.Move(d1);
                }
                if(np1 != null)
                {
                    return false;
                }
                while (np2 != null && _board[np2.X, np2.Y] != null)
                {
                    np2 = np2.Move(d2);
                }
                if(np2 != null)
                {
                    return false;
                }else
                {
                    return true;
                }
                //return _board[np2.X, np2.Y] != null;
            }
        }

        private void AddAnyValidMove(Color c, Position sourcePosition, Direction d)
        {
            Position nextPos = sourcePosition.Move(d);
            bool hasVisitedOnePiece = false;
            while (nextPos != null && _board[nextPos.X, nextPos.Y] != null && _board[nextPos.X, nextPos.Y].CurrentColor != c)
            {
                hasVisitedOnePiece = true;
                nextPos = nextPos.Move(d);
            }
            if (nextPos != null && _board[nextPos.X, nextPos.Y] != null && _board[nextPos.X, nextPos.Y].CurrentColor == c)
            {
                if (hasVisitedOnePiece)
                {
                    Move m = new Move(c, sourcePosition.X, sourcePosition.Y);
                    if (!_valid_moves[c].Contains(m))
                    {
                        _valid_moves[c].Add(m);
                    }
                }
            }
        }
        private int FlipPieces(Direction d, Piece sourcePiece, Position sourcePos)
        {
            Position nextPos = sourcePos.Move(d);
            List<Piece> piecesToFlip = new List<Piece>();
            while (nextPos != null && _board[nextPos.X, nextPos.Y] != null && _board[nextPos.X, nextPos.Y].CurrentColor != sourcePiece.CurrentColor)
            {
                piecesToFlip.Add(_board[nextPos.X, nextPos.Y]);
                nextPos = nextPos.Move(d);
            }
            if (nextPos != null && _board[nextPos.X, nextPos.Y] != null && _board[nextPos.X, nextPos.Y].CurrentColor == sourcePiece.CurrentColor)
            {
                foreach (var ptf in piecesToFlip)
                {
                    ptf.Flip();
                    ++_number_of_pieces[sourcePiece.CurrentColor];
                    --_number_of_pieces[sourcePiece.CurrentColor.Opposite()];
                }
                return piecesToFlip.Count;
            }
            return 0;
        }

        public void ComputeHeuristic(Color maxColor)
        {
            int tempHeuristic = 0;
            for (int y = 0; y < BOARD_HEIGHT; ++y)
            {
                for (int x = 0; x < BOARD_WIDTH; ++x)
                {
                    if (_board[x, y] != null)
                    {
                        Position p = new Position(x, y);
                        if (_board[x, y].CurrentColor == maxColor)
                        {
                            tempHeuristic += _Base_Position_Values[p];
                        }
                        else
                        {
                            tempHeuristic -= _Base_Position_Values[p];
                        }
                    }
                }
            }
            if(HEURISTIC == Heuristic.COMPLETE)
            {
                var fVals = ComputeFrontier();
                tempHeuristic -= fVals[maxColor];
                tempHeuristic += fVals[maxColor.Opposite()];

                var sVals = ComputeStable();
                tempHeuristic += sVals[maxColor];
                tempHeuristic -= sVals[maxColor.Opposite()];

                if (_turns_passed < 15)
                {
                    tempHeuristic -= _pieces_flipped_from_last_move;
                }

                if (60 - _turns_passed <= 10)
                {
                    var iVals = ComputeIslands();
                    if (_last_applied_move != null && _last_applied_move.MoveColor == maxColor)
                    {
                        tempHeuristic += iVals[IslandSize.EVEN] * 7;
                    }
                    else
                    {
                        tempHeuristic += iVals[IslandSize.ODD] * 7;
                    }
                }

                if (_valid_moves[Color.BLACK].Count == 0 && _valid_moves[Color.WHITE].Count == 0)
                {
                    if (_number_of_pieces[maxColor] < _number_of_pieces[maxColor.Opposite()])
                    {
                        tempHeuristic -= 200;
                    }
                    else if (_number_of_pieces[maxColor] > _number_of_pieces[maxColor.Opposite()])
                    {
                        tempHeuristic += 200;
                    }
                }
            }
            
            

            _heuristic_value = tempHeuristic;
        }

        private bool PlayMove(Move m)
        {
            if (_valid_moves[m.MoveColor].Contains(m))
            {
                Piece p = new Piece(m.MoveColor);
                _board[m.MovePosition.X, m.MovePosition.Y] = p;
                ++_number_of_pieces[m.MoveColor];
                Position sp = new Position(m.MovePosition.X, m.MovePosition.Y);
                _pieces_flipped_from_last_move = 0;
                _pieces_flipped_from_last_move += FlipPieces(Direction.DIAG_LD, p, sp);
                _pieces_flipped_from_last_move += FlipPieces(Direction.DIAG_RD, p, sp);
                _pieces_flipped_from_last_move += FlipPieces(Direction.DIAG_LU, p, sp);
                _pieces_flipped_from_last_move += FlipPieces(Direction.DIAG_RU, p, sp);
                _pieces_flipped_from_last_move += FlipPieces(Direction.UP, p, sp);
                _pieces_flipped_from_last_move += FlipPieces(Direction.DOWN, p, sp);
                _pieces_flipped_from_last_move += FlipPieces(Direction.RIGHT, p, sp);
                _pieces_flipped_from_last_move += FlipPieces(Direction.LEFT, p, sp);
                UpdateValidMoves();
                _last_applied_move = m;
                ++_turns_passed;
                return true;
            }
            return false;
        }
        #endregion

        #region Public Functions
        public void DumpValidMoves(Color movesFor)
        {
            Console.WriteLine(String.Format("Valid moves for {0}", movesFor));
            foreach (var m in _valid_moves[movesFor])
            {
                Console.WriteLine(m);
            }
        }

        public bool GetHasValidMoves(Color c)
        {
            return _valid_moves[c].Count > 0;
        }

        public IEnumerable<Board> GetChildrenBoards(Color c)
        {
            foreach (var m in _valid_moves[c])
            {
                Board b = new Board(ParameterString);
                if (b.PlayMove(m))
                {
                    yield return b;
                }
                else
                {
                    continue;
                }
            }
        }
        #endregion

        #region Member Overrides
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            for (int y = -1; y < BOARD_HEIGHT; ++y)
            {
                for (int x = -1; x < BOARD_WIDTH; ++x)
                {
                    if (y < 0 || x < 0)
                    {
                        if (x < 0)
                        {
                            if (y >= 0)
                                sb.Append(y);
                            else
                                sb.Append(' ');
                        }
                        else
                        {
                            sb.Append(x);
                        }
                    }
                    else
                    {
                        if (_board[x, y] == null)
                        {
                            sb.Append("-");
                        }
                        else
                        {
                            switch (_board[x, y].CurrentColor)
                            {
                                case Color.BLACK:
                                    sb.Append("B");
                                    break;
                                case Color.WHITE:
                                    sb.Append("W");
                                    break;
                            }
                        }
                    }

                }
                if (y < BOARD_HEIGHT - 1)
                {
                    sb.AppendLine();
                }
            }
            return sb.ToString();
        }
        #endregion

    }
}
