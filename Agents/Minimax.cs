﻿using Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agents
{
    class Minimax
    {
        private class MinmaxResult
        {
            private int _value;
            private Board _associated_board;

            public int Value
            {
                get
                {
                    return _value;
                }
                set
                {
                    _value = value;
                }
            }
            public Board AssociatedBoard
            {
                get
                {
                    return _associated_board;
                }
                set
                {
                    _associated_board = value;
                }
            }

            public MinmaxResult() { }
            public MinmaxResult(int val, Board associatedMove)
            {
                _value = val;
                _associated_board = associatedMove;
            }
        }

        private Board _initial_board;
        private Color _max_color;

        public Minimax(Board initBoard, Color maxColor)
        {
            _initial_board = initBoard;
            _max_color = maxColor;
        }

        public Board FindNextMove(int maxDepth)
        {
            if (_initial_board.GetHasValidMoves(_max_color))
            {
                var res = RecursiveMinMax(_initial_board, new Tuple<int, Step>(int.MaxValue, Step.MIN), _max_color, maxDepth, 1);
                return res.AssociatedBoard;
            }
            else
            {
                return null;
            }
            
        }

        private MinmaxResult RecursiveMinMax(Board currentBoardConf, Tuple<int, Step> parentABvalue, Color currentColor, int maxDepth, int currentDepth)
        {
            if (currentDepth == maxDepth || !currentBoardConf.GetHasValidMoves(currentColor))
            {
                currentBoardConf.ComputeHeuristic(_max_color);
                return new MinmaxResult(currentBoardConf.HeuristicValue, currentBoardConf);
            }
            else
            {
                Color nextColor = currentColor == Color.BLACK ? Color.WHITE : Color.BLACK;
                Step currentStep = parentABvalue.Item2 == Step.MAX ? Step.MIN : Step.MAX;

                MinmaxResult currentResult = new MinmaxResult();
                currentResult.Value = currentStep == Step.MAX ? int.MinValue : int.MaxValue;
                

                foreach (var child in currentBoardConf.GetChildrenBoards(currentColor))
                {
                    MinmaxResult res = RecursiveMinMax(child, new Tuple<int, Step>(currentResult.Value, currentStep), nextColor, maxDepth, currentDepth + 1);
                    if (currentStep == Step.MAX)
                    {
                        if (res.Value > currentResult.Value)
                        {
                            currentResult.Value = res.Value;
                            currentResult.AssociatedBoard = child;

                            if (parentABvalue.Item1 != int.MinValue && currentResult.Value > parentABvalue.Item1)
                            {
                                break;
                            }
                        }
                        
                    }
                    else
                    {
                        if (res.Value < currentResult.Value)
                        {
                            currentResult.Value = res.Value;
                            currentResult.AssociatedBoard = child;
                            if (parentABvalue.Item1 != int.MaxValue && currentResult.Value < parentABvalue.Item1)
                            {
                                break;
                            }
                        }
                    }
                }
                return currentResult;
            }
        }

    }
}
