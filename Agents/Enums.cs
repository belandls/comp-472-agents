﻿namespace Enumerations
{
    public enum Color
    {
        WHITE,
        BLACK
    }

    public enum Step
    {
        MAX,
        MIN
    }

    public enum Direction
    {
        RIGHT,
        LEFT,
        UP,
        DOWN,
        DIAG_LU,
        DIAG_RU,
        DIAG_LD,
        DIAG_RD
    }

    public enum IslandSize
    {
        EVEN,
        ODD
    }

    public enum Heuristic
    {
        COMPLETE,
        POSITIONNAL
    }
}