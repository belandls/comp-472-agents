﻿using Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agents
{
    static class ExtensionMethods
    {
        public static IEnumerable<String> Chunk(this string src, int chunkSize)
        {
            return Enumerable.Range(0, src.Length / chunkSize).Select((i) => src.Substring(i * chunkSize, chunkSize));
        }

        public static string Combine(this string[] srcStrs)
        {
            StringBuilder sb = new StringBuilder();
            for(int i = 0; i < srcStrs.Length; ++i)
            {
                sb.Append(srcStrs[i]);
            }
            return sb.ToString();
        }

        public static Color Opposite(this Color c)
        {
            return c == Color.BLACK ? Color.WHITE : Color.WHITE;
        }
    }
}
