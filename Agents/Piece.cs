﻿using Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agents
{
    class Piece
    {
        private Color _current_color;

        public Color CurrentColor
        {
            get
            {
                return _current_color;
            }
        }

        public Piece(Color initialColor)
        {
            _current_color = initialColor;
        }

        public void Flip()
        {
            if (_current_color == Color.BLACK)
            {
                _current_color = Color.WHITE;
            }
            else
            {
                _current_color = Color.BLACK;
            }
        }

        public override string ToString()
        {
            return _current_color.ToString();
        }
    }
}
