﻿using Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agents
{
    class Move
    {

        private Color _color;
        private Position _pos;
        public Color MoveColor 
        {
            get
            {
                return _color;
            }
        }

        public Position MovePosition
        {
            get
            {
                return _pos;
            }
        }


        public Move(Color color, int x, int y) 
        {
            _color = color;
            _pos = new Position(x, y);
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }else if(obj.GetType() != typeof(Move))
            {
                return false;
            }
            else
            {
                Move temp = (Move)obj;
                return temp._color == _color && _pos.Equals(temp._pos);
            }
        }

        public override int GetHashCode()
        {
            return _color.GetHashCode() + _pos.GetHashCode();
        }

        public override string ToString()
        {
            return String.Format("{0} - [{1},{2}]", _color, _pos.X, _pos.Y);
        }
    }
}
