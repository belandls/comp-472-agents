﻿using Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agents
{
    class Position
    {
        private const int BOARD_HEIGHT = 8;
        private const int BOARD_WIDTH = 8;
        private int _x;
        private int _y;

        public int X
        {
            get
            {
                return _x;
            }
        }

        public int Y
        {
            get
            {
                return _y;
            }
        }

        public Position(int x, int y)
        {
            _x = x;
            _y = y;
        }

        public Position Move(Direction d)
        {
            switch (d)
            {
                case Direction.UP:
                    if (_y - 1 >= 0)
                    {
                        return new Position(_x, _y - 1);
                    }
                    break;
                case Direction.DOWN:
                    if (_y + 1 < BOARD_HEIGHT)
                    {
                        return new Position(_x, _y + 1);
                    }
                    break;
                case Direction.LEFT:
                    if (_x - 1 >= 0)
                    {
                        return new Position(_x - 1, _y);
                    }
                    break;
                case Direction.RIGHT:
                    if (_x + 1 < BOARD_WIDTH)
                    {
                        return new Position(_x + 1, _y);
                    }
                    break;
                case Direction.DIAG_LU:
                    if (_y - 1 >= 0 && _x - 1 >= 0)
                    {
                        return new Position(_x - 1, _y - 1);
                    }
                    break;
                case Direction.DIAG_RU:
                    if (_y - 1 >= 0 && _x + 1 < BOARD_WIDTH)
                    {
                        return new Position(_x + 1, _y - 1);
                    }
                    break;
                case Direction.DIAG_LD:
                    if (_y + 1 < BOARD_WIDTH && _x - 1 >= 0)
                    {
                        return new Position(_x - 1, _y + 1);
                    }
                    break;
                case Direction.DIAG_RD:
                    if (_y + 1 < BOARD_WIDTH && _x + 1 < BOARD_HEIGHT)
                    {
                        return new Position(_x + 1, _y + 1);
                    }
                    break;
            }
            return null;
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }
            else if (obj.GetType() !=  typeof(Position))
            {
                return false;
            }
            else
            {
                Position temp = (Position)obj;
                return temp._y == _y && temp._x == _x;
            }
        }

        public override int GetHashCode()
        {
            return _x.GetHashCode() + _y.GetHashCode();
        }

        public override string ToString()
        {
            return String.Format("[{0},{1}]", _x, _y);
        }
    }
}
